#include <stdio.h>
#include <stdlib.h>

#include <cuda_runtime.h>

#define CUDA_SAFE_CALL(call)                                              \
    do {                                                                  \
        cudaError_t err = call;                                           \
        if (cudaSuccess != err) {                                         \
            fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
                    __FILE__, __LINE__, cudaGetErrorString(err));         \
            exit(EXIT_FAILURE);                                           \
        }                                                                 \
    } while (0)


__global__ void kernel(void) { printf("hello world\n"); }

int main(int argc, char *argv[])
{
    kernel<<<1, 1>>>();
    CUDA_SAFE_CALL(cudaDeviceSynchronize());
    return 0;
}
