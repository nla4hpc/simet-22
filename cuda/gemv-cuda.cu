#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#include <cuda_runtime.h>

#define ASSERT_CUDA_SAFE_CALL(call)                                       \
    do {                                                                  \
        cudaError_t err = call;                                           \
        if (cudaSuccess != err) {                                         \
            fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
                    __FILE__, __LINE__, cudaGetErrorString(err));         \
            exit(EXIT_FAILURE);                                           \
        }                                                                 \
    } while (0)

#define THREADS_PER_BLOCK 512


static double inline get_time();


static bool assert_equal_arrays(const std::vector<double> &x1,
                                const std::vector<double> &x2);


void reference_gemv(const int num_rows, const int num_cols, const double *mat,
                    const double *b, double *x)
{
    for (int i = 0; i < num_rows; ++i) {
        double sum = 0.0;
        for (int j = 0; j < num_cols; ++j) {
            sum += mat[i * num_cols + j] * b[j];
        }
        x[i] = sum;
    }
}


__global__ void row_parallel_gemv_kernel(const int num_rows, const int num_cols,
                                         const double *mat, const double *b,
                                         double *x)
{
    // Implement this kernel.
}


__global__ void reduction_gemv_kernel(const int num_rows, const int num_cols,
                                      const double *mat, const double *b,
                                      double *x)
{
    // Implement this kernel.
}


int main(int argc, char *argv[])
{
    auto num_reps = 50;
    double st_time = 0.0;
    double end_time = 0.0;
    double duration = 0.0;
    int num_rows = 20;
    bool verify = false;
    if (argc < 2) {
        fprintf(stderr, "Usage: %s [num_rows] --verify\n", argv[0]);
        exit(1);
    } else {
        num_rows = std::atoi(argv[1]);
        if (argc > 2) {
            std::string inarg = std::string(argv[2]);
            verify = inarg.find("verify") != std::string::npos;
        }
    }
    const int num_cols = num_rows;
    auto b = std::vector<double>(num_cols, 2.0);
    auto mat = std::vector<double>(num_rows * num_cols, 42.0);
    auto x = std::vector<double>(num_rows, 4.0);
    auto ref_x = x;
    double *dx;
    double *dmat;
    double *db;

    ASSERT_CUDA_SAFE_CALL(cudaMalloc(&dx, sizeof(double) * x.size()));
    ASSERT_CUDA_SAFE_CALL(cudaMalloc(&db, sizeof(double) * b.size()));
    ASSERT_CUDA_SAFE_CALL(cudaMalloc(&dmat, sizeof(double) * mat.size()));

    ASSERT_CUDA_SAFE_CALL(cudaMemcpy(db, b.data(), sizeof(double) * b.size(),
                                     cudaMemcpyHostToDevice));
    ASSERT_CUDA_SAFE_CALL(cudaMemcpy(
        dmat, mat.data(), sizeof(double) * mat.size(), cudaMemcpyHostToDevice));
    int num_blocks = num_rows;

    for (int reps = 0; reps < num_reps; ++reps) {
        ASSERT_CUDA_SAFE_CALL(cudaMemcpy(dx, ref_x.data(),
                                         sizeof(double) * b.size(),
                                         cudaMemcpyHostToDevice));
        ASSERT_CUDA_SAFE_CALL(cudaDeviceSynchronize());
        st_time = get_time();
        num_blocks = (num_rows / THREADS_PER_BLOCK) + 1;
        row_parallel_gemv_kernel<<<num_blocks, THREADS_PER_BLOCK>>>(
            x.size(), b.size(), dmat, db, dx);
        // Uncomment to use reduction version
        // reduction_gemv_kernel<<<num_blocks, THREADS_PER_BLOCK>>>(
        //     x.size(), b.size(), dmat, db, dx);
        ASSERT_CUDA_SAFE_CALL(cudaDeviceSynchronize());
        end_time = get_time();
        duration += (end_time - st_time);
    }

    std::cout << " Performance (Flops): "
              << (num_rows * num_cols) / (duration / num_reps) << std::endl;


    if (verify) {
        reference_gemv(ref_x.size(), b.size(), mat.data(), b.data(),
                       ref_x.data());

        ASSERT_CUDA_SAFE_CALL(cudaMemcpy(
            x.data(), dx, sizeof(double) * x.size(), cudaMemcpyDeviceToHost));
        auto err = assert_equal_arrays(x, ref_x);
        if (err) {
            std::cout << " Problem size (" << num_rows << " , " << num_cols
                      << ") Gemv result is correct" << std::endl;
        } else {
            std::cout << " Problem size (" << num_rows << " , " << num_cols
                      << ") Gemv result is not correct" << std::endl;
        }
    }

    ASSERT_CUDA_SAFE_CALL(cudaFree(dx));
    ASSERT_CUDA_SAFE_CALL(cudaFree(db));
    ASSERT_CUDA_SAFE_CALL(cudaFree(dmat));

    return 0;
}


double inline get_time()
{
    timespec tp;
    int err;
    err = clock_gettime(CLOCK_MONOTONIC, &tp);

    if (err != 0) {
        std::fprintf(stderr, "ERROR: clock_gettime failed with error %d: %s\n",
                     err, strerror(err));
        std::exit(1);
    }

    return tp.tv_sec + tp.tv_nsec * 1e-9;
}


bool assert_equal_arrays(const std::vector<double> &x1,
                         const std::vector<double> &x2)
{
    if (x1.size() != x2.size()) {
        return false;
    }
    for (size_t i = 0; i < x1.size(); ++i) {
        if (x1[i] != x2[i]) {
            return false;
        }
    }
    return true;
}
