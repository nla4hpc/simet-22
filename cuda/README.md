This example demonstrates the some simple Dense kernels on CUDA.

## Building

Running `make` will generate

+ `hw-cuda` : a CUDA hello world example,
+ `axpy-cuda` CUDA parallelized axpy on the GPU,
+ `gemv-cuda` CUDA parallelized gemv on the GPU,

Build binaries separately:

+ `make hw-cuda`
+ `make axpy-cuda`
+ `make gemv-cuda`

## Running the executables

You can just run the executables by:

+ `./hw-cuda`
+ `./axpy-cuda [size] [--verify]`
+ `./gemv-cuda [num_rows] [--verify]`
