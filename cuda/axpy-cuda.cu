#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#include <cuda_runtime.h>

#define ASSERT_CUDA_SAFE_CALL(call)                                       \
    do {                                                                  \
        cudaError_t err = call;                                           \
        if (cudaSuccess != err) {                                         \
            fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n", \
                    __FILE__, __LINE__, cudaGetErrorString(err));         \
            exit(EXIT_FAILURE);                                           \
        }                                                                 \
    } while (0)

#define THREADS_PER_BLOCK 512

#define NUM_BLOCKS 56


static double inline get_time();


static bool assert_equal_arrays(const std::vector<double> &x1,
                                const std::vector<double> &x2);


void reference_axpy(const int num_elems, const double alpha, const double *x,
                    double *y)
{
    // Implement this kernel.
}


__global__ void axpy_kernel(const int size, const double alpha, const double *x,
                            double *y)
{
    // Implement this kernel.
}


int main(int argc, char *argv[])
{
    auto num_reps = 50;
    double st_time = 0.0;
    double end_time = 0.0;
    double duration = 0.0;
    int size = 20;
    bool verify = false;
    if (argc < 2) {
        fprintf(stderr, "Usage: %s [size] --verify\n", argv[0]);
        exit(1);
    } else {
        size = std::atoi(argv[1]);
        if (argc > 2) {
            std::string inarg = std::string(argv[2]);
            verify = inarg.find("verify") != std::string::npos;
        }
    }
    auto x = std::vector<double>(size, 2.0);
    auto y = std::vector<double>(size, 4.0);
    auto ref_y = y;
    double alpha = 0.7;
    double *dx;
    double *dy;

    ASSERT_CUDA_SAFE_CALL(cudaMalloc(&dx, sizeof(double) * size));
    ASSERT_CUDA_SAFE_CALL(cudaMalloc(&dy, sizeof(double) * size));

    ASSERT_CUDA_SAFE_CALL(cudaMemcpy(dy, y.data(), sizeof(double) * size,
                                     cudaMemcpyHostToDevice));
    ASSERT_CUDA_SAFE_CALL(cudaMemcpy(dx, x.data(), sizeof(double) * size,
                                     cudaMemcpyHostToDevice));

    for (int reps = 0; reps < num_reps; ++reps) {
        ASSERT_CUDA_SAFE_CALL(cudaMemcpy(dy, y.data(), sizeof(double) * size,
                                         cudaMemcpyHostToDevice));
        ASSERT_CUDA_SAFE_CALL(cudaDeviceSynchronize());
        st_time = get_time();
        axpy_kernel<<<NUM_BLOCKS, THREADS_PER_BLOCK>>>(size, alpha, dx, dy);
        ASSERT_CUDA_SAFE_CALL(cudaDeviceSynchronize());
        end_time = get_time();
        duration += (end_time - st_time);
    }

    std::cout << " Performance (Flops): " << (2 * size) / (duration / num_reps)
              << std::endl;


    if (verify) {
        reference_axpy(x.size(), alpha, x.data(), ref_y.data());
        ASSERT_CUDA_SAFE_CALL(cudaMemcpy(y.data(), dy, sizeof(double) * size,
                                         cudaMemcpyDeviceToHost));
        auto err = assert_equal_arrays(y, ref_y);
        if (err) {
            std::cout << " Axpy result is correct" << std::endl;
        } else {
            std::cout << " Axpy result is not correct" << std::endl;
        }
    }

    ASSERT_CUDA_SAFE_CALL(cudaFree(dx));
    ASSERT_CUDA_SAFE_CALL(cudaFree(dy));

    return 0;
}


double inline get_time()
{
    timespec tp;
    int err;
    err = clock_gettime(CLOCK_MONOTONIC, &tp);

    if (err != 0) {
        std::fprintf(stderr, "ERROR: clock_gettime failed with error %d: %s\n",
                     err, strerror(err));
        std::exit(1);
    }

    return tp.tv_sec + tp.tv_nsec * 1e-9;
}


bool assert_equal_arrays(const std::vector<double> &x1,
                         const std::vector<double> &x2)
{
    if (x1.size() != x2.size()) {
        return false;
    }
    for (size_t i = 0; i < x1.size(); ++i) {
        if (x1[i] != x2[i]) {
            return false;
        }
    }
    return true;
}
