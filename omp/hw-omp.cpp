#include <cstdio>

#include <omp.h>  //< For OpenMP functions eg. omp_get_thread_num()


int main(int argc, char *argv[])
{
#pragma omp parallel num_threads(5)
    {
        const int ID = omp_get_thread_num();
        printf("Hello world from thread: %d\n", ID);
    }
    return 0;
}
