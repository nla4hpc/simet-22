#include <time.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>


static double inline get_time();


static bool assert_equal_arrays(const std::vector<double> &x1,
                                const std::vector<double> &x2);


void reference_gemv(const int num_rows, const int num_cols, const double *mat,
                    const double *b, double *x)
{
    // Implement this kernel.
}


void row_parallel_gemv(const int num_rows, const int num_cols,
                       const double *mat, const double *b, double *x)
{
    // Implement this kernel.
}


void reduction_gemv(const int num_rows, const int num_cols, const double *mat,
                    const double *b, double *x)
{
    // Implement this kernel.
}


int main(int argc, char *argv[])
{
    int num_rows = 20;
    bool verify = false;
    if (argc < 2) {
        fprintf(stderr, "Usage: %s [num_rows] --verify\n", argv[0]);
        exit(1);
    } else {
        num_rows = std::atoi(argv[1]);
        if (argc > 2) {
            std::string inarg = std::string(argv[2]);
            verify = inarg.find("verify") != std::string::npos;
        }
    }
    int num_cols = num_rows;
    auto b = std::vector<double>(num_cols, 2.0);
    auto mat = std::vector<double>(num_rows * num_cols, 42.0);
    auto x = std::vector<double>(num_rows, 4.0);
    auto ref_x = x;
    auto num_reps = 50;
    double st_time = 0.0;
    double end_time = 0.0;
    double duration = 0.0;

    for (int reps = 0; reps < num_reps; ++reps) {
        x = ref_x;
        st_time = get_time();
        // Uncomment to use row parallel version
        row_parallel_gemv(x.size(), b.size(), mat.data(), b.data(), x.data());
        // reduction_gemv(x.size(), b.size(), mat.data(), b.data(), x.data());
        end_time = get_time();
        duration += (end_time - st_time);
    }

    std::cout << " Performance (Flops): "
              << (num_rows * num_cols) / (duration / num_reps) << std::endl;

    if (verify) {
        reference_gemv(ref_x.size(), b.size(), mat.data(), b.data(),
                       ref_x.data());
        auto err = assert_equal_arrays(x, ref_x);
        if (!err) {
            std::cout << " GEMV result is incorrect" << std::endl;
        } else {
            std::cout << " All GEMV tests passed " << std::endl;
        }
    }
    return 0;
}


// helper function definitions


double inline get_time()
{
    timespec tp;
    int err;
    err = clock_gettime(CLOCK_MONOTONIC, &tp);

    if (err != 0) {
        std::fprintf(stderr, "ERROR: clock_gettime failed with error %d: %s\n",
                     err, strerror(err));
        std::exit(1);
    }

    return tp.tv_sec + tp.tv_nsec * 1e-9;
}


bool assert_equal_arrays(const std::vector<double> &x1,
                         const std::vector<double> &x2)
{
    if (x1.size() != x2.size()) {
        return false;
    }
    for (size_t i = 0; i < x1.size(); ++i) {
        if (x1[i] != x2[i]) {
            return false;
        }
    }
    return true;
}
