This example demonstrates the some simple Dense kernels on OMP.

## Building

You can set the number of threads with the environment variable `OMP_NUM_THREADS`, on linux:
`export OMP_NUM_THREADS=4`

Running `make` will generate

+ `hw-omp` : a OMP hello world example,
+ `axpy-omp` OMP parallelized axpy on the CPU,
+ `gemv-omp` OMP parallelized gemv on the CPU,

Build binaries separately:

+ `make hw-omp`
+ `make axpy-omp`
+ `make gemv-omp`

## Running the executables

You can just run the executables by:

+ `./hw-omp`
+ `./axpy-omp [size] [--verify]`
+ `./gemv-omp [num_rows] [--verify]`
