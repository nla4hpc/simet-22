#include <time.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>


static double inline get_time();


static bool assert_equal_arrays(const std::vector<double> &x1,
                                const std::vector<double> &x2);


void reference_axpy(const int num_elems, const double alpha, const double *x,
                    double *y)
{
    // Implement this kernel.
}


void axpy(const int num_elems, const double alpha, const double *x, double *y)
{
    // Implement this kernel.
}


int main(int argc, char *argv[])
{
    auto num_reps = 50;
    double st_time = 0.0;
    double end_time = 0.0;
    double duration = 0.0;
    int size = 20;
    bool verify = false;
    if (argc < 2) {
        fprintf(stderr, "Usage: %s [size] --verify\n", argv[0]);
        exit(1);
    } else {
        size = std::atoi(argv[1]);
        if (argc > 2) {
            std::string inarg = std::string(argv[2]);
            verify = inarg.find("verify") != std::string::npos;
        }
    }
    auto x = std::vector<double>(size, 2.0);
    auto y = std::vector<double>(size, 4.0);
    auto ref_y = y;
    double alpha = 0.7;

    for (int reps = 0; reps < num_reps; ++reps) {
        y = ref_y;
        st_time = get_time();
        axpy(x.size(), alpha, x.data(), y.data());
        end_time = get_time();
        duration += (end_time - st_time);
    }

    std::cout << " Performance (Flops): " << (2 * size) / (duration / num_reps)
              << std::endl;


    if (verify) {
        reference_axpy(x.size(), alpha, x.data(), ref_y.data());
        auto err = assert_equal_arrays(y, ref_y);
        if (err) {
            std::cout << " Axpy result is correct" << std::endl;
        } else {
            std::cout << " Axpy result is not correct" << std::endl;
        }
    }
    return 0;
}


double inline get_time()
{
    timespec tp;
    int err;
    err = clock_gettime(CLOCK_MONOTONIC, &tp);

    if (err != 0) {
        std::fprintf(stderr, "ERROR: clock_gettime failed with error %d: %s\n",
                     err, strerror(err));
        std::exit(1);
    }

    return tp.tv_sec + tp.tv_nsec * 1e-9;
}


bool assert_equal_arrays(const std::vector<double> &x1,
                         const std::vector<double> &x2)
{
    if (x1.size() != x2.size()) {
        return false;
    }
    for (size_t i = 0; i < x1.size(); ++i) {
        if (x1[i] != x2[i]) {
            return false;
        }
    }
    return true;
}
